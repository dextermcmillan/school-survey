const puppeteer = require('puppeteer'); //Duh.
const converter = require('json-2-csv'); // Handles conversion from returned JSON to CSV so it can be saved.
const fs = require('fs'); //Saves csv file to local drive.
const input = require('.input/input.js'); // Loads some useful functions.
const functions = require('./functions.js'); // Loads some useful functions.

const settings = {
  saveAs: "output-bc",
  textToAppend: "staff directory",
  selectorWithContent: '#rso > div > div:nth-child(1) div.yuRUbf > a',
  startItem: 1
}

let emailNumber = 0


function run () {
    return new Promise(async (resolve, reject) => {


      //This is boilerplate Puppeteer code that launches the browser. Settings are defined here.
      const browser = await puppeteer.launch({
        headless:false, // Makes scraper run headless or not.
        defaultViewport: null, // Makes sure the viewport matches (roughly) what it would be if you went there on your commputer (ie. full screen).
        slowMo:0 // Slows down Puppeteer so things don't break.
      });

      //Launch a new page.
      const page = await browser.newPage();

      let allData = []

      if (settings.startItem !== 1) {
        let rawdata = await fs.readFileSync('./output/' + settings.saveAs + '.json');
        allData = await JSON.parse(rawdata);
        console.log(`Previously gathered data loaded from ./output/${settings.saveAs}.json`)
      }

      for (i = settings.startItem; i < input.values.length; i++)  {
        const results = {
          item: "",
          result: "",
          likelyStaffPage: "",
        };

        const url = 'https://www.google.com' //The URL to scrape. In this case, it goes to Google.
        await page.goto(url, {
    		  waitUntil: 'networkidle0',
    		});

        const searchPhrase = input.values[i] + ' ' + settings.textToAppend;

        await page.$eval('input[name=q]', (el, value) => el.value = value, searchPhrase);
        await Promise.all([
          page.waitForNavigation({timeout:0}),
          page.keyboard.press('Enter'),
          page.waitForSelector(settings.selectorWithContent + " > h3", {timeout:0})
        ]);

        const text = await page.$eval(settings.selectorWithContent + " > h3", el => el.textContent.toString().toLowerCase())

        results.result = await page.$eval(settings.selectorWithContent, el => el.href)

        // await Promise.all([
        //   page.waitForNavigation({waitUntil: 'domcontentloaded'}),
        //   page.click(settings.selectorWithContent),
        // ]);

        // const pageText = await page.content()
        // const regex = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi
        // results.emails = [...new Set(await pageText.match(regex))]

        results.item = input.values[i];


        if (text.includes('staff') || results.result.includes('staff') ) {
          results.likelyStaffPage = "Yes"
        }

        allData.push(results)

        // emailNumber+=results.emails.length
        // console.log(`\n${results.item}: ${results.emails.length} emails found.`)
        // console.log(`Total emails found: ${emailNumber}`)
        console.log(`Schools done: ${allData.length}`)
        if (allData.length % 10 === 0) {
          console.log("\x1b[31m", `${allData.length} results found. Saving data...` ,'\x1b[0m')
          await functions.saveData(allData, settings.saveAs, true, true)
        }
      }
      browser.close();
      return resolve(allData);

  })
}

//This executes the above defined code and saves to the file destination specified above.
run().catch(console.error);
