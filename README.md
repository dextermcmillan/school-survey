# School Survey Project
### Date: March 16, 2021

Easily run through a list of items, Google each one, and grab information from the page. Useful for grabbing addresses or contact information.

### Notes
* Earlier versions of the scraper (for BC and AB) ran the scrape in two separate files. The first file, called "basic" googles the name of the school and gets what is likely the staff directory. The second file, called "emails", runs through the list of URLs gathered by the first file, grabs the content of the page, and regex searches for all email addresses.
* For Saskatchewan, the basic and email scripts were combined into one script that does it all.
* For commented code, see ./schools-sask.js. Other files have not been commented thoroughly.
