const puppeteer = require('puppeteer'); //Duh.
const converter = require('json-2-csv'); // Handles conversion from returned JSON to CSV so it can be saved.
const fs = require('fs'); //Saves csv file to local drive.
const functions = require('./functions.js'); // Loads some useful functions.

// Defines some parameters for the scrape.
const settings = {
  saveAs: "output-sask",
  textToAppend: "saskatchewan staff directory",
  selectorWithContent: '#rso > div > div:nth-child(1) div.yuRUbf > a',
  startItem: 1,
}
// Used to track the number of emails gathered so far.
let emailNumber = 0

// Pulls in JSON created from a list of schools, to which emails will be added onto.
let rawdata = fs.readFileSync('./input/input-sask.json');
let input = JSON.parse(rawdata);

// This is the function that is called at the end of this script.
function run () {
    return new Promise(async (resolve, reject) => {

      //This is boilerplate Puppeteer code that launches the browser. Settings are defined here.
      const browser = await puppeteer.launch({
        // When you're Googling, you need to run headless, because Google will issue reCAPTCHA challenges you need to solve.
        headless:false,
        defaultViewport: null,
        slowMo:0
      });

      //Launch a new page.
      const page = await browser.newPage();

      // All the data will be dumped here.
      let allData = []

      // If the page crashes and we change the startPage in the settings object to a page other than one, we load progress into allData before we continue.
      if (settings.startItem !== 1) {
        let rawdata = await fs.readFileSync('./output/' + settings.saveAs + '.json');
        allData = await JSON.parse(rawdata);
        console.log(`Previously gathered data loaded from ./output/${settings.saveAs}.json`)
      }

      // For each school object in the schools array...
      for (i = settings.startItem; i < input.length; i++)  {

        // Launch Google and wait for it to load.
        const url = 'https://www.google.com'
        await page.goto(url, {
    		  waitUntil: 'networkidle0',
    		});

        // Build the phrase to be entered into the search box, and enter it.
        const searchPhrase = input[i].school + ' ' + settings.textToAppend;
        await page.$eval('input[name=q]', (el, value) => el.value = value, searchPhrase);

        // Click the "I'm Feeling Lucky" button and wait for navigation.
        await Promise.all([
          page.waitForNavigation({timeout:0}),
          page.click('.FPdoLc > center:nth-child(1) > input:nth-child(2)'),
        ]);

        // This detects whether we're presented with a reCAPTCHA challenge, in which case we need to wait while we solve it. Should happen once every 20-35 iterations.
        await page.waitForFunction('!(document.location.href.includes("www.google.com/sorry/"))', {timeout:0})

        // Grab the staff directory page.
        input[i].staffDirectory = await page.url()

        // Grab all page text and regex search for email addresses. The new Set removes any duplicates that might be present in the emails retrieved.
        const pageText = await page.content()
        const regex = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi
        input[i].emails = [...new Set(await pageText.match(regex))]

        // Was using this as a flag for whether or not Google actually found us a staff page. In many cases it did not. It's not overly important.
        if (pageText.includes('staff') || input[i].staffDirectory.includes('staff') ) {
          input[i].likelyStaffPage = "Yes"
        }

        // Push the appended school object, now with emails, to the allData array and log it, along with our progress.
        allData.push(input[i])
        console.log(input[i])
        console.log(`Schools done: ${allData.length}`)

        // This will save our allData object after collecting 10 new schools. We do this in case the browser crashes.
        // The functions.saveData function converts the JSON to CSV each time as well. This could probably be made more efficient.
        if (allData.length % 10 === 0) {
          console.log("\x1b[31m", `${allData.length} results found. Saving data...` ,'\x1b[0m')
          await functions.saveData(allData, settings.saveAs, true, true)
        }
      }

      // Closes the browser and resolves the promise.
      browser.close();
      return resolve(allData);
  })
}

//This executes the above defined code.
run().catch(console.error);
