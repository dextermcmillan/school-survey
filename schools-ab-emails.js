const puppeteer = require('puppeteer'); //Duh.
const converter = require('json-2-csv'); // Handles conversion from returned JSON to CSV so it can be saved.
const fs = require('fs'); //Saves csv file to local drive.
const functions = require('./functions.js'); // Loads some useful functions.

const settings = {
  saveAs: "output-ab-emails",
  selectorWithContent: '#rso > div > div:nth-child(1) > div > div.yuRUbf > a',
  startItem: 941
}

const saveAs = 'output.csv'; //File path for outputted data.

let rawdata = fs.readFileSync('./output/output-ab.json');
const schoolsArray = JSON.parse(rawdata);



let schools = schoolsArray.filter(function (el) {
    return el.likelyStaffPage === "Yes";
});


let emailNumber = 0;

function run () {
    return new Promise(async (resolve, reject) => {

      let allData = []

      if (settings.startItem !== 1) {
        let rawdata = await fs.readFileSync('./output/' + settings.saveAs + '.json');
        allData = await JSON.parse(rawdata);
        console.log(`Previously gathered data loaded from ./output/${settings.saveAs}.json`)
      }

      console.log(allData.length)
      //This is boilerplate Puppeteer code that launches the browser. Settings are defined here.
      const browser = await puppeteer.launch({
        headless:true, // Makes scraper run headless or not.
        defaultViewport: null, // Makes sure the viewport matches (roughly) what it would be if you went there on your commputer (ie. full screen).
        slowMo:0 // Slows down Puppeteer so things don't break.
      });

      //Launch a new page.
      const page = await browser.newPage();

      for (i = settings.startItem-1; i < schools.length; i++)  {

        try {
          await page.goto(schools[i].result, {
            waitUntil: 'networkidle0',
          });
        } catch {
          schools[i].errors = "Page didn't load."
        }

        const pageText = await page.content()
        const regex = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi
        schools[i].emails = [...new Set(await pageText.match(regex))]

        allData.push(schools[i])
        console.log(schools[i])

        emailNumber+=schools[i].emails.length
        console.log(`\n${schools[i].item}: ${schools[i].emails.length} emails found.`)
        console.log(`Total emails found: ${emailNumber}`)
        console.log(`Schools done: ${allData.length}`)
        if (allData.length % 10 === 0) {
          console.log("\x1b[31m", `${allData.length} results found. Saving data...` ,'\x1b[0m')
          await functions.saveData(allData, settings.saveAs, true, true)
        }
      }
      browser.close();
      return resolve(allData);
  })
}

//This executes the above defined code and saves to the file destination specified above.
run().catch(console.error);
